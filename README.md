# Blank dockerized website project

## Description
With SSL, Nginx reverse proxy, wordpress website and postfix smtp for wordpress mail system.

There is a version without the SSL and the reverse proxy inside the `docker-compose.yml.no_https`

## SSL
Obtain ssl certificate using certbot :
```
certbot certonly
```

Renewing the certificate:
```
certbot renew
```

## Wordpress
Advised wordpress plugin:
 - Contact form 7
 - WP Add Custom CSS
 - WPS Hide Login
 - Yoast SEO
 - MonsterInsights
